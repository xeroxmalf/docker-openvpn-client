#!/bin/bash
## copy in mullvad configs
cp /config/mullvad*.conf /etc/openvpn/mullvad.conf
cp /config/mullvad_ca.crt /etc/openvpn/mullvad_ca.crt
cp /config/mullvad_crl.pem /etc/openvpn/mullvad_crl.pem
cp /config/mullvad_userpass.txt /etc/openvpn/mullvad_userpass.txt
cp /config/update-resolv-conf /etc/openvpn/update-resolv-conf
chmod 755 /etc/openvpn/update-resolv-conf

## start openvpn
cd /etc/openvpn
nohup openvpn --config /etc/openvpn/mullvad.conf
