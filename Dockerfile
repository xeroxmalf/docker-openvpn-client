FROM alpine:3.8
RUN apk add --no-cache \
curl \
nano \
openvpn

ADD start.sh /etc/openvpn/start.sh
ENTRYPOINT ["/etc/openvpn/start.sh"]
